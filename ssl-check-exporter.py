from http.server import HTTPServer, BaseHTTPRequestHandler
from urlparse import urlparse
from prometheus_client.core import GaugeMetricFamily, REGISTRY
from datetime import datetime
import time
from OpenSSL import crypto as c

def get_ssl_time(cert_file):
  cert = c.load_certificate(c.FILETYPE_PEM, file(cert_file).read())
  return time.mktime(datetime.strptime(cert.get_notAfter(), "%Y%m%d%H%M%SZ").timetuple())

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        out_fe = "ssl_certificate_file_exist{"
        out_c = "ssl_certificate_check{"
        query = urlparse(self.path).query
        if query != '':
          file = query.split('=')[1]
          try:
            open(file,'r')
            out_fe = out_fe + "file=\"" + file + "\"" + "} 1"
            out_c = out_c + "file=\"" + file + "\"" + "} " + str(get_ssl_time(file))
          except IOError:
            out_fe = out_fe + "file=\"" + file + "\"" + "} 0"
            out_c = out_c + "file=\"" + file + "\"" + "} 0"
        else:
          file = "query error"
        self.send_response(200)
        self.end_headers()
        self.wfile.write(out_fe + "\n" + out_c)


httpd = HTTPServer(('0.0.0.0', 8001), SimpleHTTPRequestHandler)
httpd.serve_forever()
